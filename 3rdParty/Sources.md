Type-C.kicad_sym: https://github.com/ai03-2725/Type-C.pretty/tree/master
HRO-TYPE-C-31-M-12.kicad_mod: https://github.com/ai03-2725/Type-C.pretty/tree/master
HRO TYPE-C-31-M-12.step: https://github.com/ai03-2725/Type-C.pretty/tree/master

blackpill v2.0.step: https://grabcad.com/library/black-pill-v2-0-1#!

SW_SPST_B3S-1000.step: https://components.omron.com/eu-en/products/switches/B3S
