# Custom KiCad Library

This repository contains the KiCad libraries with some additional own and 3rd party items.

## Setup

Change the KiCad paths in the paths settings dialog (Schematic -> Configure Paths)
Add a new environment variable `KICAD_LIBRARY_DIR` with the path to the repository

Change the following environment variables to the following values
- KICAD7_3DMODEL_DIR	    ${KICAD_LIBRARY_DIR}/kicad-packages3D
- KICAD7_3RD_PARTY	        ${KICAD_LIBRARY_DIR}/3rdParty
- KICAD7_FOOTPRINT_DIR	    ${KICAD_LIBRARY_DIR}/kicad-footprints
- KICAD7_SYMBOL_DIR	        ${KICAD_LIBRARY_DIR}/kicad-symbols
- KICAD7_TEMPLATE_DIR	    ${KICAD_LIBRARY_DIR}/template
- KICAD_USER_TEMPLATE_DIR	${KICAD_LIBRARY_DIR}/template

Now the libraries are found in KiCad

## Structure

The `kicad-footprints`, `kicad-symbols` and `kicad-packages3D` are forks of the original KiCad libraries with additional own created items. When creating new items store them here

### 3rdParty

All third party items are stored in the 3rdParty folder to distinguish them from the others. Check the license before checking in new files!




